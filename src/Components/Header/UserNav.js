import React from "react";
import { useSelector } from "react-redux";
import { userReducer } from "./../../redux/reducer/userReducer";
import { userLocalService, USER_LOCAL } from './../../service/localService';

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.user;
  });

  const handleLogOut = () => {
    userLocalService.remove();
    window.location.reload();
  }

  const renderContent = () => {
    console.log("user");
    if (user) {
      return (
        <div>
          <span>{user?.hoTen}</span>
          <button onClick={handleLogOut} className="border-2 border-black px-5 py-2 rounded">
            Đăng xuất
          </button>
        </div>
      );
    } else {
      return (
        <>
          <button onClick={() => {
            window.location.href = "/login" 
          }}
          className="border-2 border-black px-5 py-2 rounded"
          >
            Đăng nhập
          </button>
          <button
          className="border-2 border-black px-5 py-2 rounded"
          >
            Đăng kí
          </button>
        </>
      );
    }
  };

  return <div>
    {renderContent()}
  </div>
}



