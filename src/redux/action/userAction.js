import { SET_USER_LOGIN } from "./../constant/userConstant";
import { message } from "antd";
import { postLogin } from "./../../service/userService";

export const setUserAction = (values) => {
  return {
    type: SET_USER_LOGIN,
    payload: values,
  };
};

export const setUserActionService = (values, onSuccess) => {
  return (dispatch) => {
    postLogin(values)
      .then((res) => {
        message.success("Đăng nhập thành công");
        dispatch({
          type: SET_USER_LOGIN,
          payload: values,
        });
        onSuccess();
      })
      .catch((err) => {
        message.success("Đăng nhập thất bại");
      });
  };
};
